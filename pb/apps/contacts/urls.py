from django.conf.urls import patterns, url


urlpatterns = patterns('pb.apps.contacts.views',
    url(r'^details/$', 'details', name='details'),
)
