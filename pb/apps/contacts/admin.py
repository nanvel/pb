from django.contrib import admin
from django.utils.translation import ugettext as _

from mptt.admin import MPTTModelAdmin

from pb.apps.core.utils import AdminURLs

from .models import Category, Phone, Man, ManPhone


def phone_users(obj):
    html = ''
    for user in obj.users.all():
        html += '<a href="%s?phone__number=%d&man__id=%d">%s</a><br>' % (
                AdminURLs.url(ManPhone),
                obj.number,
                user.id,
                unicode(user))
    html += '<a href="%s?phone=%d"><b>+ man</b></a>' % (
                AdminURLs.url(ManPhone, action='add'),
                obj.id)
    return html
phone_users.short_description = _('People')
phone_users.allow_tags = True

class PhoneAdmin(admin.ModelAdmin):
    list_display= ('number', 'room', 'category', 'details', phone_users)
    search_fields = ('number', 'room', 'category__title', 'details',
                                        'users__lname', 'users__fname')
    list_per_page = 10


def user_phones(obj):
    html = ''
    for phone in obj.phones.all():
        html += '<a href="%s?phone__number=%d&man__id=%d">%s</a><br>' % (
                AdminURLs.url(ManPhone),
                phone.number,
                obj.id,
                phone.number)
    html += '<a href="%s?man=%d"><b>+ phone</b></a>' % (
                AdminURLs.url(ManPhone, action='add'),
                obj.id)
    return html
user_phones.short_description = _('Phones')
user_phones.allow_tags = True

def user_photo(obj):
    html = ''
    if obj.photo:
        html += '<img src="%s">' % obj.photo.url
    else:
        html += _('No photo')
    return html
user_photo.short_description = _('Photo')
user_photo.allow_tags = True

class ManAdmin(admin.ModelAdmin):
    list_display= ('__unicode__', 'position', user_photo, user_phones)
    search_fields = ('lname', 'fname', 'position', 'phones__number')
    list_per_page = 10


class ManPhoneAdmin(admin.ModelAdmin):
    raw_id_fields = ('phone', 'man')

    def lookup_allowed(self, key, *args, **kwargs):
        if key in ('phone__number', 'man__id',):
            return True
        return super(ManPhoneAdmin, self).lookup_allowed(key, *args, **kwargs)


admin.site.register(Category, MPTTModelAdmin)
admin.site.register(Phone, PhoneAdmin)
admin.site.register(Man, ManAdmin)
admin.site.register(ManPhone, ManPhoneAdmin)
