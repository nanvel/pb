# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ManPhone'
        db.create_table(u'contacts_manphone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('man', self.gf('django.db.models.fields.related.ForeignKey')(related_name='man_phones', to=orm['contacts.Man'])),
            ('phone', self.gf('django.db.models.fields.related.ForeignKey')(related_name='phone_users', to=orm['contacts.Phone'])),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal(u'contacts', ['ManPhone'])

        # Adding unique constraint on 'ManPhone', fields ['man', 'phone']
        db.create_unique(u'contacts_manphone', ['man_id', 'phone_id'])

        # Adding model 'Man'
        db.create_table(u'contacts_man', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('mname', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal(u'contacts', ['Man'])

        # Adding model 'Category'
        db.create_table(u'contacts_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=250)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['contacts.Category'])),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'contacts', ['Category'])

        # Adding model 'Phone'
        db.create_table(u'contacts_phone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='phones', to=orm['contacts.Category'])),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True)),
            ('details', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
            ('room', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'contacts', ['Phone'])


    def backwards(self, orm):
        # Removing unique constraint on 'ManPhone', fields ['man', 'phone']
        db.delete_unique(u'contacts_manphone', ['man_id', 'phone_id'])

        # Deleting model 'ManPhone'
        db.delete_table(u'contacts_manphone')

        # Deleting model 'Man'
        db.delete_table(u'contacts_man')

        # Deleting model 'Category'
        db.delete_table(u'contacts_category')

        # Deleting model 'Phone'
        db.delete_table(u'contacts_phone')


    models = {
        u'contacts.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['contacts.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'contacts.man': {
            'Meta': {'ordering': "('lname', 'fname')", 'object_name': 'Man'},
            'fname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mname': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phones': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'users'", 'symmetrical': 'False', 'through': u"orm['contacts.ManPhone']", 'to': u"orm['contacts.Phone']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'contacts.manphone': {
            'Meta': {'unique_together': "(('man', 'phone'),)", 'object_name': 'ManPhone'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'man': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'man_phones'", 'to': u"orm['contacts.Man']"}),
            'phone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'phone_users'", 'to': u"orm['contacts.Phone']"})
        },
        u'contacts.phone': {
            'Meta': {'ordering': "('number',)", 'object_name': 'Phone'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'phones'", 'to': u"orm['contacts.Category']"}),
            'details': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'room': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['contacts']