from ..models import Category, Phone, Man, ManPhone


class CategoryFactory:

    counter = 0

    @classmethod
    def create(self, parent=None):
        self.counter += 1
        title = 'Some category %d' % self.counter
        if parent:
            return Category.objects.create(title=title, parent=parent)
        return Category.objects.create(title=title)


class PhoneFactory:

    counter = 0

    @classmethod
    def create(self, category=None):
        self.counter += 1
        if not category:
            category=CategoryFactory.create()
        return Phone.objects.create(
                    category=category,
                    number=12345 + self.counter,
                    room='0303%d' % self.counter)


class ManFactory:

    counter = 0

    @classmethod
    def create(self, phone=None):
        self.counter += 1
        fname = 'yuki%d' % self.counter
        lname = 'nagato%d' % self.counter
        man = Man.objects.create(lname=lname, fname=fname)
        if phone:
            ManPhone.objects.create(man=man, phone=phone)
        return man
