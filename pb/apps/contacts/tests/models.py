from django.test import TestCase

from ..models import Category, Phone, Man, ManPhone
from .factories import CategoryFactory, PhoneFactory


class ModelsTestCase(TestCase):

    def test_category(self):
        category = CategoryFactory.create()
        subcategory = Category.objects.create(
                                    title='Some subcategory',
                                    parent=category)
        self.assertEqual(Category.objects.count(), 2)
        self.assertEqual(unicode(category), category.title)
        self.assertEqual(
                    category.get_absolute_url(),
                    '/?category=%d' % category.pk)

    def test_phone(self):
        category = CategoryFactory.create()
        phone = Phone.objects.create(
                                    category=category,
                                    number='12345',
                                    room='0303')
        self.assertEqual(Phone.objects.count(), 1)
        self.assertEqual(unicode(phone), phone.number)

    def test_man(self):
        man = Man.objects.create(lname='Hamasaki', fname='Ayumi')
        self.assertEqual(Man.objects.count(), 1)
        with self.settings(NAME_PATTERN=u'{fname} {lname} {mname}'):
            self.assertEqual(unicode(man), u'Ayumi Hamasaki')
        self.assertEqual(man.phones.count(), 0)
        phone = PhoneFactory.create()
        ManPhone.objects.create(man=man, phone=phone)
        self.assertEqual(man.phones.count(), 1)
