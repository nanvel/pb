from django.shortcuts import render_to_response
from django.conf import settings
from django.db.models import Q
from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from pb.apps.core.decorators import render_to

from .models import Category, Phone
from .forms import CategoryForm, SearchForm, ManDetailsForm


def index(request):
    q = Q()
    category_form = CategoryForm(request.GET or None)
    if category_form.is_valid():
        category = category_form.cleaned_data['category']
        q &= Q(category__in=category.get_descendants(include_self=True))
    else:
        category = None
    search_form = SearchForm(request.GET or None)
    if search_form.is_valid():
        q &= search_form.cleaned_data['q']
    results = Phone.objects.filter(q)
    paginator = Paginator(results, settings.DEFAULT_ITEMS_PER_PAGE)
    page = request.GET.get('page') 
    try:
        results = paginator.page(page) 
    except PageNotAnInteger:
        results = paginator.page(1) 
    except EmptyPage:
        results = paginator.page(paginator.num_pages)
    categories = Category.objects.all()
    ctx = {
        'categories': categories,
        'category': category,
        'results': results,
        'paginator': paginator}
    return render_to_response('index.html', ctx)


@render_to('details.html')
def details(request):
    """
    returns man details
    """
    if not request.is_ajax():
        raise Http404
    form = ManDetailsForm(request.GET)
    if not form.is_valid():
        raise Http404
    return {'man': form.cleaned_data['man']}
