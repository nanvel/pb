(function($, undefined) {
    $(function() {
        /* init search field */
        var get_query = URI.parseQuery(window.location.search);
        if('q' in get_query) {
            $('input[name=q]').val(get_query['q']);
            $('.js-search-breadcrumb').append(' / ' + get_query['q']);
        };
        if('q' in get_query || 'category' in get_query) {
            $('body').removeClass('index');
        };
        if('category' in get_query) {
            $('.js-categories-link').append(' / ' + $('.js-categories').find('.selected').text());
        };
        $('body').removeClass('hide');
        /* init search button */
        $('.js-search').click(function(){
            $(this).parents('form').submit();
            return false;
        });
        $('.js-categories-link').click(function(){
            $link = $(this);
            $link.blur();
            if($('.js-categories').is(':visible')) {
                $('.js-categories').stop().slideUp(400, function(){
                    $link.find('.js-arrow').text('▾');
                });
            } else {
                $('.js-categories').css({height: $(window).height() - parseInt($('.js-categories').css('top'), 10) - 60 + 'px'});
                $('.js-categories').stop().slideDown(400, function(){
                    $link.find('.js-arrow').text('▴');
                });
            }
            return false;
        });
        /* overlay on loading */
        window.onbeforeunload = function() {
            $('a, .btn').blur();
            $('.js-loading').show();
            return undefined;
        };
        /* user details tooltip */
        init_tooltips();
    });
    function init_tooltips() {
        $('.js-user').click(function() {
            var $link = $(this);
            var url = $link.parents('table').data('details-url');
            $link.blur();
            $('.js-user-tooltip').remove();
            var posX = $(window).width() + $(window).scrollLeft() - $link.offset().left + 10;
            var posY = $link.offset().top - $(window).scrollTop() - 5;
            var $tooltip = $('<div class="js-user-tooltip user-tooltip"></div>').appendTo($('body'));
            $tooltip.css({top: posY, right: posX});
            $tooltip.addClass('spinner').show();
            $.ajax({
                url: url,
                dataType: "html",
                data: {pk: $link.data('pk')},
                type: 'GET',
                success: function (data) {
                    $tooltip.removeClass('spinner').html(data);
                },
            });
            return false;
        }).mouseout(function() {
            $('.js-user-tooltip').remove();
        });
    }
})(jQuery);
