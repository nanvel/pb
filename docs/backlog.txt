Iteration 1:
+ ticket:1 init
+ ticket:2 base models
+ tocket:3 tests for models, configure admin
+ ticket:4 install pipline and bootstrap styles
+ ticket:5 initial html
+ ticket:6 header with search bar
+ ticket:7 view data for specified category
+ ticket:9 pagination (tests for view)
+ ticket:10 show phone details
+ ticket:11 search
+ ticket:12 resizing photo while uploading
+ ticket:13 admin improvements (search, display)
+ ticket:14 redesign, footer
+ ticket:15 styles: search input colors
+ ticket:16 styles: breadcrumbs
+ ticket:17 styles: categories as breadcrumbs
+ ticket:18 refactor less files
+ ticket:19 show loading state
+ ticket:20 styles: no results
+ ticket:21 styles: footer
+ ticket:22 people details popup
+ ticket:23 scroll doesn't work in webkit
+ ticket:24 show results from subcategories
+ ticket:25 seo improvements, add favicon
+ ticket:26 do something with items count in selects (admin)
+ ticket:27 i18n
Finished 2013.03.23


Iteration 2:
ticket: error handlers
ticket: save latest numbers to cache
ticket: editor
ticket: export to csv/pdf
ticket: items per page
ticket: database dump from admin
