.. pb project settings

Names format
============

Names format can be changed by NAME_PATTERN variable. See ``settings/variables.py``.

By default:

::

    NAME_PATTERN = '{lname} {fname} {mname}'
