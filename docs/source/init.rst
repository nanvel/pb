.. pb init

Init
====

Base
----

::

    virtualenv .env --no-site-packages
    source .env/bin/activate
    pip install -r requirements.txt
    cp pb/settings/local.py.default pb/settings/local.py
    make syncdb
    make migrate
    make run

pipeline configuration
----------------------

install lessc (I use lessc writed in ruby instead if java version):

::

    sudo apt-get install rubygems1.8 ruby1.8-dev
    sudo gem install rubygems-update
    sudo gem update rubygems
    sudo gem install less
    sudo ln -s /var/lib/gems/1.8/bin/lessc /usr/bin/

Additional for Debian:

::

    sudo gem install therubyracer

Install yui-compressor:

::

    sudo apt-get install yui-compressor

Check is right paths to binaries are specified:

::

    # settings/pipeline.py
    PIPELINE_YUI_BINARY = '/usr/bin/yui-compressor'
    PIPELINE_LESS_BINARY = '/usr/local/bin/lessc'
