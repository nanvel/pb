.. pb documentation master file, created by
   sphinx-quickstart on Sat Mar  9 11:40:26 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pb's documentation!
==============================

Contents:

.. toctree::
    :maxdepth: 2

    init.rst
    settings.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

