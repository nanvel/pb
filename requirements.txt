Django==1.5
South==0.7.6
Sphinx==1.1.3
django-mptt==0.5.5
PIL==1.1.7
django-pipeline==1.3.6
